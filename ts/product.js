var Product = /** @class */ (function () {
    function Product() {
    }
    Object.defineProperty(Product.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (v) {
            this._id = v;
        },
        enumerable: true,
        configurable: true
    });
    return Product;
}());
var Connguoi = /** @class */ (function () {
    function Connguoi() {
    }
    //cách 1
    Connguoi.prototype.setname = function (name) {
        this.name = name;
    };
    Object.defineProperty(Connguoi.prototype, "Name", {
        //cách 2
        set: function (new_name) {
            this.name = new_name;
        },
        enumerable: true,
        configurable: true
    });
    return Connguoi;
}());
var nguoithu1 = new Connguoi();
nguoithu1.set_name("Vietpro");
// hoặc
nguoithu1.Name = "Vietpro";
